* System Management with Nix

This is my attempt to creat a reproducable build of my system(s) using
Nix.  Currently the main system is my laptop (Jake) which has a full
NixOS install.  I am experimenting with using the nix package manager
on my Ubunutu VDI at work.

The hope is to build some kind of homogony between my configurations at
home and at work, and in the process reduce the amount of wasted
effort in reconfiguring what I do.

** home-manager

I'm using home manager to configure my user environment. I intend this
to be portable so that I can have a consistent home environment across
a number of systems.

*** emacs

The emacs configuration is the first time I have really started to
peice together nix as a system.  The configuration has largly been
taken from
https://gitlab.com/rycee/configurations/-/blob/745260f1069b90156396d88797c9e56cfe484170/user/modules/programs/emacs-init.nix
but I have spent time copying it out line for line understanding what
it does. I now have a resonable understanding what it does and I
mostly understand the why. I think I would struggle writing something
like this from scratch but I think I know enough to be able to adapt
it to my own needs.

