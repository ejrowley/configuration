{config, lib, pkgs, ... }:
with lib;
let
  emacsPackageType =
    types.coercedTo                              # converts
      types.str                                  # a string
      (pkgName: epkgs: [ epkgs.${pkgName} ])     # using this function
      (types.nullOr types.unspecified);          # into a null or unspecified

  usePackageType = types.submodule ({ name, config, ...}: {
    options = {
      enable = mkEnableOption "Emacs package ${name}";

      package = mkOption {
	      type = emacsPackageType;
	      default = name;
	      defaultText = "<name>";
	      description = ''
	  The package to use for this module
	'';
      };

      defer = mkOption {
	      type = types.either types.bool types.ints.positive;
	      default = false;
	      description = ''
	  The <option>:defer<option> setting.
	'';
      };

      demand = mkOption {
	      type = types.bool;
	      default = false;
	      description = ''
	  The <option>:demand<option> setting.
	'';
      };

      diminish = mkOption {
	      type = types.listOf types.str;
	      default = [];
	      description = ''
	  The <option>:diminish<option> setting.
	'';
      };

      mode = mkOption {
	      type = types.listOf types.str;
	      default = [];
	      description = ''
	  The <option>:mode<option> setting.
	'';
      };

      after = mkOption {
	      type = types.listOf types.str;
	      default = [];
	      description = ''
	  The <option>:after<option> setting.
	'';
      };

      bind = mkOption {
	      type = types.attrsOf types.str;
	      default = {};
	      example = { "M-<up>" = "drag-stuff-up"; "M-<down>" = "drag-stuff-down"; };
	      description = ''
	  The entries to use for <option>:bind</option>.
	'';
      };

      bindLocal = mkOption {
	      type = types.attrsOf (types.attrsOf types.str);
	      default = {};
	      example = { helm-command-map = { "C-c h" = "helm-execute-persistent-action"; }; };
	      description = ''
	  The entries to use for local keymaps in <option>:bind</option>.
	'';
      };

      bindKeyMap = mkOption {
	      type = types.attrsOf types.str;
	      default = {};
	      example = { "C-c p" = "projectile-command-map"; };
	      description = ''
	  The entries to use for <option>:bind-keymap</option>.
	'';
      };

      command = mkOption {
	      type = types.listOf types.str;
	      default = [];
	      description = ''
	  The entries to use for <option>:commands</option>.
	'';
      };

      config = mkOption {
	      type = types.lines;
	      default = "";
	      description = ''
	  Code to place in the <option>:config</option> section.
	'';
      };

      init = mkOption {
	      type = types.lines;
	      default = "";
	      description = ''
	  Code to place in the <option>:init</option> section.
	'';
      };

      custom = mkOption {
	      type = types.lines;
	      default = "";
	      description = ''
	  Code to place in the <option>:custom</option> section.
	'';
      };

      extraConfig = mkOption {
	      type = types.lines;
	      default = "";
	      description = ''
	  Additional lines to place in the use-package configuration.
	'';
      };

      hook = mkOption {
	      type = types.listOf types.str;
	      default = [];
	      description = ''
	  The entries to use for <option>:hook</option>.
	'';
      };

      assembly = mkOption {
	      type = types.lines;
	      readOnly = true;
	      internal = true;
	      description = "The assembled use-package code.";
      };
    };

    config = mkIf config.enable {
      assembly =
	      let
	        quoted = v: ''"${escape ["\""] v}"'';
	        mkBindHelper = cmd: prefix: bs:
	          optionals (bs != {}) (
	                           [ ":${cmd} (${prefix}" ]
	                           ++ mapAttrsToList (n: v: "  (${quoted n} . ${v})") bs
	                           ++ [ ")" ]
	                         );
	        mkAfter = vs: optional (vs != []) ":after (${toString vs})";
	        mkCommand = vs: optional (vs != []) ":commands (${toString vs})";
	        mkDiminish = vs: optional (vs != []) ":diminish (${toString vs})";
	        mkMode = map (v: ":mode ${v}");
	        mkBind = mkBindHelper "bind" "";
	        mkBindLocal = bs:
	          let
	            mkMap = n: v: mkBindHelper "bind" ":map ${n}" v;
	          in
	            flatten (mapAttrsToList mkMap bs);
	        mkBindKeyMap = mkBindHelper "bind-keymap" "";
	        mkHook = map (v: ":hook ${v}");
	        mkCustom = map (v: ":custom ${v}");
	        mkDefer = v:
	          if isBool v then optional v ":defer t"
	          else [ ":defer ${toString v}" ];
	        mkDemand = v: optional v ":demand t";
	      in
	        concatStringsSep "\n  " (
	          [ "(use-package ${name}" ]
	          ++ mkAfter config.after
	          ++ mkBind config.bind
	          ++ mkBindKeyMap config.bindKeyMap
	          ++ mkBindLocal config.bindLocal
	          ++ mkCommand config.command
	          ++ mkDefer config.defer
	          ++ mkDiminish config.diminish
	          ++ mkHook config.hook
	          ++ mkMode config.mode
	          ++ optionals (config.custom != "") [ ":custom" config.custom ]
	          ++ optionals (config.init != "") [ ":init" config.init ]
	          ++ optionals (config.config != "") [ ":config" config.config ]
	          ++ optionals (config.extraConfig != "") config.extraConfig
	        ) + ")";
    };
  });

  usePackageSetup = ''
    (eval-when-compile
      (require 'package)

      (setq package-archives nil
            package-enable-at-startup nil
            package--init-file-ensured t)
      (require 'use-package)
      (setq use-package-verbose nil))
      (require 'diminish)
      (require 'bind-key)
  '';

  initFile = ''
    ;;; hm-init.el --- Emacs configuration built from nix and Home Manager
    ;;
    ;; -*- lexical-binding: t; -*-
    ;;
    ;;; Commentary:
    ;;
    ;; A configuration generated from a Nix by Home Manager.
    ;;
    ;;; Code:

    ${usePackageSetup}

    (setq inhibit-startup-message t
          inhibit-startup-echo-area-message (user-login-name))

    (setq initial-major-mode 'fundamental-mode
          initial-scratch-message nil)

    ;; Disable some GUI distractions

    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    (menu-bar-mode -1)
    (blink-cursor-mode 0)

    (line-number-mode)
    (column-number-mode)
    (size-indication-mode)

    (setq-default indent-tabs-mode nil
                  tab-width 4
                  c-basic-offset 4)

    (setq-default show-trailing-whitespace t)
  ''
  + concatStringsSep "\n\n"
    (map (getAttr "assembly")
      (filter (getAttr "enable")
        (attrValues config.programs.emacs.init.usePackage)))
  + ''
  (provide 'hm-init)
  '';

in
{
  options.programs.emacs.init = {
    enable = mkEnableOption "Emacs configuration";

    usePackage = mkOption {
      type = types.attrsOf usePackageType;
      default = {};
      example = listeralExample ''
        {
          dhall-mode = {
            mode = [ ''"\\.dhall\\'"'''];
          };
        }
      '';
    };
  };

  config.services.emacs.enable = true;

  /*
  This is realising a file hm-init
  using the trivialBuild of the emacsPackagesNgGen.

  emacsPackagesNgGen is part of the Emacs packages framework
  this lays down the file - I assume in the emacs load path
  */
  config = {
    home.packages = [
      ((pkgs.emacsPackagesNgGen config.programs.emacs.finalPackage).trivialBuild {
	      pname = "hm-init";
	      version = "0";
	      src = pkgs.writeText "hm-init.el" initFile;
      })
    ];

    programs.emacs.enable = true;

    programs.emacs.init = {
      enable = true;
    };

    programs.emacs.init.usePackage = {
      company = {
        enable = true;
      };

      evil = {
        enable = true;
	      config = ''
          (evil-mode 1)
        '';
      };

      evil-magit = {
        enable = true;
      };

      flycheck = {
        enable = true;
        hook = [''
        (after-init . global-flycheck-mode)
        ''];
      };

      hydra = {
        enable = true;
      };

      ivy = {
        enable = true;
        config = ''
          (ivy-mode 1)
          (setq ivy-use-virtual-buffers t)
         (setq enable-recursive-minibuffers t)
        '';
      };

      lsp-mode = {
        enable = true;
      };

      lsp-ui = {
        enable = true;
      };

      lsp-java = {
        enable = true;
        config = ''
          (add-hook 'java-mode-hook 'lsp)
        '';
      };

      dap-mode = {
        enable = true;
        after = ["lsp-mode"];
        config = ''
          (dap-mode t)
          (dap-auto-config-mode)
          (require 'dap-java)
        '';
      };

      magit = {
        enable = true;
        bind = {
          "C-c g" = "magit-status";
        };
      };

      nix-mode = {
        enable = true;
	      mode = [ ''"\\.nix\\'"'' ];
      };

      nov = {
        enable = true;
        config = ''
          (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
        '';
      };

      org-journal = {
        enable = true;
      };

      org-roam = {
        enable = true;
        hook = [''
        (after-init . org-roam-mode)
        ''];
        custom = ''
        (org-roam-directory "~/org/")
        '';
        bind = {
          "C-c n l" = "org-roam";
          "C-c n f" = "org-roam-find-file";
          "C-c n g" = "org-roam-show-graph";
        };
      };

      projectile = {
        enable = true;
        bind = {
          "C-c p" = "projectile-command-map";
        };
        config = ''
          (projectile-mode +1)
        '';
      };

      which-key = {
        enable = true;
        config = ''
          (which-key-mode)
        '';
      };

      yasnippet = {
        enable = true;
        config = ''
          (yas-global-mode)
        '';
      };

      solarized-theme = {
        enable = true;
        config = ''
          (load-theme 'solarized-light t)
        '';
      };
    };

    programs.emacs.extraPackages = epkgs:
      let
        getPkg = v:
          if isFunction v then [ (v epkgs) ]
          else optional (isString v && hasAttr v epkgs) epkgs.${v};
      in
        [
          epkgs.bind-key
          epkgs.diminish
          epkgs.use-package
        ] ++ (
          concatMap(v: getPkg (v.package))
            (builtins.attrValues config.programs.emacs.init.usePackage )
        );

    home.file.".emacs.d/init.el".text = ''
      (require 'hm-init)
      (provide 'init)
    '';
  };
}
