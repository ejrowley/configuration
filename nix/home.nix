{ config, lib, pkgs, ... }:

let
  homeDir = builtins.getEnv "HOME";

in {
  imports = [ ./emacs.nix ];

  nixpkgs.config = {
    allowUnfree = true;
  };

  programs.home-manager.enable = true;

  home.stateVersion = "20.03";

  programs = {

    alacritty = {
      enable = true;
      settings = lib.attrsets.recursiveUpdate (import ./program/terminal/alacritty/default-settings.nix) {
      };
    };

    vim = {
      enable = true;
      settings = {
        expandtab = true;
        background = "light";
        smartcase = true;
      };
      plugins = [
        pkgs.vimPlugins.vim-airline
        pkgs.vimPlugins.typescript-vim
        pkgs.vimPlugins.vim-go
        pkgs.vimPlugins.nerdtree
        pkgs.vimPlugins.fugitive
      ];
    };

    starship = {
      enable = true;
      enableZshIntegration = true;
    };

    zsh = {
      enable = true;
      enableAutosuggestions = true;

    };
  };
  gtk = {
    enable = true;
    theme = {
      package = pkgs.arc-theme;
      name = "Ark-Dark";
    };

    iconTheme = {
      package = pkgs.paper-icon-theme;
      name = "Paper";
    };
  };
}
